import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import MyButton from '../../util/MyButton';

// MUI stuff
import { Menu, MenuItem, IconButton, Tooltip, Typography, Badge } from '@material-ui/core';

// Icons
import {
	Notifications as NotificationsIcon,
	Favorite as FavouriteIcon,
	Chat as ChatIcon
} from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { markNotificationsRead } from '../../redux/actions/user.actions';

class Notifications extends Component {
	state = { anchorEl: null };

	handleOpen = (event) => {
		this.setState({ anchorEl: event.target });
	};
	handleClose = () => {
		this.setState({ anchorEl: null });
	};
	onMenuOpened = () => {
		let unreadNotificationsIds = this.props.notifications
			.filter((notification) => !notification.read)
			.map((notification) => notification.notificationId);
		this.props.markNotificationsRead(unreadNotificationsIds);
	};

	render() {
		const notifications = this.props.notifications;
		const anchorEl = this.state.anchorEl;

		dayjs.extend(relativeTime);

		let notificationIcon;
		if (notifications && notifications.length > 0) {
			notifications.filter((notification) => notification.read === false).length > 0
				? (notificationIcon = (
						<Badge
							badgeContent={
								notifications.filter((notification) => notification.read === false)
									.length
							}
							color="secondary"
						>
							<NotificationsIcon />
						</Badge>
				  ))
				: (notificationIcon = <NotificationsIcon />);
		} else {
			notificationIcon = <NotificationsIcon />;
		}

		let notificationsMarkup =
			notifications && notifications.length > 0 ? (
				notifications.map((notification) => {
					const verb = notification.type === 'like' ? 'liked' : 'commented on';
					const time = dayjs(notification.createdAt).fromNow();
					const iconColor = notification.read ? 'primary' : 'secondary';
					const icon =
						notification.type === 'like' ? (
							<FavouriteIcon color={iconColor} style={{ marginRight: 10 }} />
						) : (
							<ChatIcon color={iconColor} style={{ marginRight: 10 }} />
						);

					return (
						<MenuItem key={notification.createdAt} onClick={this.handleClose}>
							{icon}
							<Typography
								component={Link}
								color="inherit"
								variant="body1"
								to={`/users/${notification.recipient}/scream/${
									notification.screamId
								}`}
							>
								{notification.sender} {verb} your scream {time}
							</Typography>
						</MenuItem>
					);
				})
			) : (
				<MenuItem onClick={this.handleClose}>You have no notifications yet</MenuItem>
			);

		return (
			<React.Fragment>
				<Tooltip placement="bottom" title="Notifications">
					<IconButton
						aria-owns={anchorEl ? 'simple-menu' : undefined}
						aria-haspopup="true"
						onClick={this.handleOpen}
					>
						{notificationIcon}
					</IconButton>
				</Tooltip>
				<Menu
					anchorEl={anchorEl}
					open={Boolean(anchorEl)}
					onClose={this.handleClose}
					onEntered={this.onMenuOpened}
				>
					{notificationsMarkup}
				</Menu>
			</React.Fragment>
		);
	}
}
Notifications.propTypes = {
	markNotificationsRead: PropTypes.func.isRequired,
	notifications: PropTypes.array.isRequired
};

const mapstateToProps = (state) => ({
	notifications: state.user.notifications
});

export default connect(
	mapstateToProps,
	{ markNotificationsRead }
)(Notifications);
