import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MyButton from '../../util/MyButton';

// MUI stuff
import { AppBar, Toolbar, Button } from '@material-ui/core';

// Icons
import { Home as HomeIcon } from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import PostScream from '../scream/PostScream';
import Notifications from './Notifications';

class NavBar extends Component {
	render() {
		const { authenticated } = this.props;

		return (
			<AppBar>
				<Toolbar className="nav-container">
					{authenticated ? (
						<React.Fragment>
							<PostScream />
							<Link to="/">
								<MyButton tip="Home">
									<HomeIcon color="inherit" />
								</MyButton>
							</Link>
							<Notifications />
						</React.Fragment>
					) : (
						<React.Fragment>
							<Button color="inherit" component={Link} to="/login">
								Login
							</Button>
							<Button color="inherit" component={Link} to="/">
								Home
							</Button>
							<Button color="inherit" component={Link} to="/signup">
								Signup
							</Button>
						</React.Fragment>
					)}
				</Toolbar>
			</AppBar>
		);
	}
}

NavBar.propTypes = {
	authenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
	authenticated: state.user.authenticated
});

export default connect(mapStateToProps)(NavBar);
