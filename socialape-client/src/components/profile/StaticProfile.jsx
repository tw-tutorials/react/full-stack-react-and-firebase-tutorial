import React from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import { Link as MuiLink, Paper, Typography } from '@material-ui/core';
// Icons
import {
	CalendarToday as CalendarTodayIcon,
	LocationOn as LocationOnIcon,
	Link as LinkIcon
} from '@material-ui/icons';

const styles = (theme) => ({
	paper: {
		padding: 20
	},
	profile: {
		'& .image-wrapper': {
			textAlign: 'center',
			position: 'relative'
		},
		'& .profile-image': {
			width: 200,
			height: 200,
			objectFit: 'cover',
			maxWidth: '100%',
			borderRadius: '50%'
		},
		'& .profile-details': {
			textAlign: 'center',
			'& span, svg': {
				verticalAlign: 'middle'
			},
			'& a': {
				color: theme.palette.primary.main
			}
		},
		'& hr': {
			border: 'none',
			margin: '0 0 10px 0'
		},
		'& svg.button': {
			'&:hover': {
				cursor: 'pointer'
			}
		}
	},
	buttons: {
		textAlign: 'center',
		'& a': {
			margin: '20px 10px'
		}
	}
});

const StaticProfile = (props) => {
	const {
		classes,
		profile: { handle, createdAt, imageUrl, bio, website, location }
	} = props;

	return (
		<Paper className={classes.paper}>
			<div className={classes.profile}>
				<div className="image-wrapper">
					<img src={imageUrl} alt="profile" className="profile-image" />
				</div>
				<hr />
				<div className="profile-details">
					<MuiLink component={Link} to={`/users/${handle}`} color="primary" variant="h5">
						@{handle}
					</MuiLink>
					<hr />
					{bio && <Typography variant="body2">{bio}</Typography>}
					<hr />
					{location && (
						<React.Fragment>
							<LocationOnIcon color="primary" />
							<span>{location}</span>
							<hr />
						</React.Fragment>
					)}
					{website && (
						<React.Fragment>
							<LinkIcon color="primary" />
							<a href={website} target="_blank" rel="noopener noreferrer">
								{' '}
								{website}
								<hr />
							</a>
						</React.Fragment>
					)}
					<React.Fragment>
						<CalendarTodayIcon color="primary" />{' '}
						<span> Joined {dayjs(createdAt).format('MMM YYYY')}</span>
					</React.Fragment>
				</div>
			</div>
		</Paper>
	);
};
StaticProfile.propTypes = {
	classes: PropTypes.object.isRequired,
	profile: PropTypes.object.isRequired
};

export default withStyles(styles)(StaticProfile);
