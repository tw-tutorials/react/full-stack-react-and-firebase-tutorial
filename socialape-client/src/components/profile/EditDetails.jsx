import React, { Component } from 'react';
import PropTypes from 'prop-types';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import {
	Dialog,
	DialogTitle,
	DialogContent,
	TextField,
	DialogActions,
	Button
} from '@material-ui/core';

// Icons
import { Edit as EditIcon } from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { editUserDetails } from '../../redux/actions/user.actions';
import MyButton from '../../util/MyButton';

const styles = (theme) => ({
	textField: { margin: '10px auto' },
	button: { float: 'right' }
});

class EditDetails extends Component {
	state = {
		data: {
			bio: '',
			website: '',
			location: ''
		},
		open: false
	};

	componentDidMount() {
		const { credentials } = this.props;
		this.mapUserDetailsToState(credentials);
	}

	handleDialogOpen = () => {
		this.setState({ open: true });
		this.mapUserDetailsToState(this.props.credentials);
	};
	handleDialogClose = () => {
		this.setState({ open: false });
	};

	handleSubmit = () => {
		this.props.editUserDetails(this.state.data);
		this.handleDialogClose();
	};
	handleChange = ({ currentTarget: target }) => {
		const data = { ...this.state.data };
		data[target.name] = target.value;
		this.setState({ data });
	};

	mapUserDetailsToState = (credentials) => {
		this.setState({
			data: {
				bio: credentials.bio ? credentials.bio : '',
				website: credentials.website ? credentials.website : '',
				location: credentials.location ? credentials.location : ''
			}
		});
	};

	render() {
		const { classes } = this.props;

		return (
			<React.Fragment>
				<MyButton
					tip="Edit details"
					onClick={this.handleDialogOpen}
					btnClassName={classes.button}
				>
					<EditIcon color="primary" />
				</MyButton>
				<Dialog
					open={this.state.open}
					onClose={this.handleDialogClose}
					fullWidth
					maxWidth="sm"
				>
					<DialogTitle>Edit your details</DialogTitle>
					<DialogContent>
						<form>
							<TextField
								name="bio"
								type="text"
								label="Bio"
								multiline
								rows="3"
								placeholder="A short bio about yourself"
								className={classes.textField}
								value={this.state.data.bio}
								onChange={this.handleChange}
								fullWidth
							/>
							<TextField
								name="website"
								type="text"
								label="Website"
								placeholder="Your personal/professional website"
								className={classes.textField}
								value={this.state.data.website}
								onChange={this.handleChange}
								fullWidth
							/>
							<TextField
								name="location"
								type="text"
								label="Location"
								placeholder="Where you live"
								className={classes.textField}
								value={this.state.data.location}
								onChange={this.handleChange}
								fullWidth
							/>
						</form>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleDialogClose} color="primary">
							Cancel
						</Button>
						<Button onClick={this.handleSubmit} color="primary">
							Save
						</Button>
					</DialogActions>
				</Dialog>
			</React.Fragment>
		);
	}
}

EditDetails.propTypes = {
	classes: PropTypes.object.isRequired,
	editUserDetails: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	credentials: state.user.credentials
});

export default connect(
	mapStateToProps,
	{ editUserDetails }
)(withStyles(styles)(EditDetails));
