import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import MyButton from '../../util/MyButton';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import { Button, Paper, Link as MuiLink, Typography } from '@material-ui/core';

// Icons
import {
	LocationOn as LocationOnIcon,
	Link as LinkIcon,
	CalendarToday as CalendarTodayIcon,
	Edit as EditIcon,
	KeyboardReturn as LogoutIcon
} from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { logoutUser, uploadImage } from '../../redux/actions/user.actions';
import EditDetails from './EditDetails';
import ProfileSkeleton from '../../util/ProfileSkeleton';

const styles = (theme) => ({
	paper: {
		padding: 20
	},
	profile: {
		'& .image-wrapper': {
			textAlign: 'center',
			position: 'relative',
			'& button': {
				position: 'absolute',
				top: '80%',
				left: '70%'
			}
		},
		'& .profile-image': {
			width: 200,
			height: 200,
			objectFit: 'cover',
			maxWidth: '100%',
			borderRadius: '50%'
		},
		'& .profile-details': {
			textAlign: 'center',
			'& span, svg': {
				verticalAlign: 'middle'
			},
			'& a': {
				color: theme.palette.primary.main
			}
		},
		'& hr': {
			border: 'none',
			margin: '0 0 10px 0'
		},
		'& svg.button': {
			'&:hover': {
				cursor: 'pointer'
			}
		}
	},
	buttons: {
		textAlign: 'center',
		'& a': {
			margin: '20px 10px'
		}
	}
});

class Profile extends Component {
	handleImageChange = (e) => {
		const image = e.target.files[0];
		const formData = new FormData();
		formData.append('image', image, image.name);

		this.props.uploadImage(formData);
	};
	handleEditPicture = () => {
		const fileInput = document.querySelector('#imageInput');
		fileInput.click();
	};
	handleLogout = () => {
		this.props.logoutUser();
	};

	render() {
		const {
			classes,
			user: {
				credentials: { handle, createdAt, imageUrl, bio, website, location },
				loading,
				authenticated
			}
		} = this.props;

		let profileMarkup = !loading ? (
			authenticated ? (
				<Paper className={classes.paper}>
					<div className={classes.profile}>
						<div className="image-wrapper">
							<img src={imageUrl} alt="profile" className="profile-image" />
							<input
								type="file"
								id="imageInput"
								onChange={this.handleImageChange}
								hidden
							/>
							<MyButton
								tip="Edit profile picture"
								onClick={this.handleEditPicture}
								btnClassName="button"
							>
								<EditIcon color="primary" />
							</MyButton>
						</div>
						<hr />
						<div className="profile-details">
							<MuiLink
								component={Link}
								to={`/users/${handle}`}
								color="primary"
								variant="h5"
							>
								@{handle}
							</MuiLink>
							<hr />
							{bio && <Typography variant="body2">{bio}</Typography>}
							<hr />
							{location && (
								<React.Fragment>
									<LocationOnIcon color="primary" />
									<span>{location}</span>
									<hr />
								</React.Fragment>
							)}
							{website && (
								<React.Fragment>
									<LinkIcon color="primary" />
									<a href={website} target="_blank" rel="noopener noreferrer">
										{' '}
										{website}
										<hr />
									</a>
								</React.Fragment>
							)}
							<React.Fragment>
								<CalendarTodayIcon color="primary" />{' '}
								<span> Joined {dayjs(createdAt).format('MMM YYYY')}</span>
							</React.Fragment>
						</div>
						<MyButton tip="Logout" onClick={this.handleLogout}>
							<LogoutIcon color="primary" />
						</MyButton>

						<EditDetails />
					</div>
				</Paper>
			) : (
				<Paper className={classes.paper}>
					<Typography variant="body2" align="center">
						No profile found, please login again
					</Typography>
					<div className={classes.buttons}>
						<Button variant="outlined" color="primary" component={Link} to="/login">
							Login
						</Button>
						<Button variant="outlined" color="secondary" component={Link} to="/signup">
							Signup
						</Button>
					</div>
				</Paper>
			)
		) : (
			<ProfileSkeleton />
		);

		return profileMarkup;
	}
}

Profile.propTypes = {
	classes: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired,
	logoutUser: PropTypes.func.isRequired,
	uploadImage: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user
});

const mapActionsToProps = { logoutUser, uploadImage };

export default connect(
	mapStateToProps,
	mapActionsToProps
)(withStyles(styles)(Profile));
