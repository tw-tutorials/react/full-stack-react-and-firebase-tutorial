import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MyButton from '../../util/MyButton';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, DialogTitle, DialogActions } from '@material-ui/core';

// Icons
import { DeleteOutline as DeleteIcon } from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { deleteScream } from '../../redux/actions/data.actions';

const styles = {
	deleteButton: { position: 'absolute', top: '10%', left: '90%' }
};

class DeleteScream extends Component {
	state = { dialogOpen: false };

	handleDialogOpen = () => {
		this.setState({ dialogOpen: true });
	};
	handleDialogClose = () => {
		this.setState({ dialogOpen: false });
	};

	deleteScream = () => {
		this.props.deleteScream(this.props.screamId);
		this.setState({ dialogOpen: false });
	};

	render() {
		const { classes } = this.props;
		return (
			<React.Fragment>
				<MyButton
					tip="Delete scream"
					onClick={this.handleDialogOpen}
					btnClassName={classes.deleteButton}
				>
					<DeleteIcon color="error" />
				</MyButton>
				<Dialog
					open={this.state.dialogOpen}
					onClose={this.handleDialogClose}
					fullWidth
					maxWidth="sm"
				>
					<DialogTitle>Are you sure you want to delete this scream?</DialogTitle>
					<DialogActions>
						<Button onClick={this.handleDialogClose} color="primary">
							Cancel
						</Button>
						<Button onClick={this.deleteScream} color="secondary">
							Delete
						</Button>
					</DialogActions>
				</Dialog>
			</React.Fragment>
		);
	}
}

DeleteScream.propTypes = {
	classes: PropTypes.object.isRequired,
	deleteScream: PropTypes.func.isRequired,
	screamId: PropTypes.string.isRequired
};

export default connect(
	null,
	{ deleteScream }
)(withStyles(styles)(DeleteScream));
