import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import PropTypes from 'prop-types';
import DeleteScream from './DeleteScream';
import ScreamDialog from './ScreamDialog';
import MyButton from '../../util/MyButton';
import LikeButton from './LikeButton';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import { Card, CardMedia, CardContent, Typography } from '@material-ui/core';

// Icons
import { Chat as CommentsIcon } from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';

const styles = {
	card: { position: 'relative', display: 'flex', marginBottom: 20 },
	image: { minWidth: 200 },
	content: { padding: 25, objectFit: 'cover' }
};

class Scream extends Component {
	render() {
		dayjs.extend(relativeTime);
		const {
			classes,
			scream: { body, commentCount, createdAt, likeCount, screamId, userHandle, userImage },
			user: {
				authenticated,
				credentials: { handle }
			}
		} = this.props;

		const deleteButton =
			authenticated && userHandle === handle ? <DeleteScream screamId={screamId} /> : null;

		return (
			<Card className={classes.card}>
				{userImage && (
					<CardMedia image={userImage} title="Profile image" className={classes.image} />
				)}
				<CardContent className={classes.content}>
					<Typography
						variant="h5"
						component={Link}
						to={`/users/${userHandle}`}
						color="inherit"
					>
						{userHandle}
					</Typography>

					{/* Delete */}
					{deleteButton}

					<Typography variant="body2" color="textSecondary">
						{dayjs(createdAt).fromNow()}
					</Typography>
					<Typography variant="body1">{body}</Typography>

					<LikeButton screamId={screamId} />
					<span>{likeCount} Likes</span>

					{/* Comments */}
					<MyButton tip="Comments">
						<CommentsIcon color="inherit" />
					</MyButton>
					<span>{commentCount} Comments</span>

					<ScreamDialog
						screamId={screamId}
						userHandle={userHandle}
						openDialog={this.props.openDialog}
					/>
				</CardContent>
			</Card>
		);
	}
}

Scream.propTypes = {
	classes: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired,
	scream: PropTypes.object.isRequired,
	openDialog: PropTypes.bool
};

const mapStateToProps = (state) => ({
	user: state.user
});

export default connect(mapStateToProps)(withStyles(styles)(Scream));
