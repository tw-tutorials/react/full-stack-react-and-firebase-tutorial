import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MyButton from '../../util/MyButton';
import { Link } from 'react-router-dom';

// Icons
import {
	FavoriteRounded as LikedIcon,
	FavoriteBorderRounded as UnlikedIcon
} from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { likeScream, unlikeScream } from '../../redux/actions/data.actions';

class LikeButton extends Component {
	likedScream = () => {
		if (
			this.props.user.likes &&
			this.props.user.likes.find((like) => like.screamId === this.props.screamId)
		) {
			return true;
		} else {
			return false;
		}
	};

	likeScream = () => {
		this.props.likeScream(this.props.screamId);
	};
	unlikeScream = () => {
		this.props.unlikeScream(this.props.screamId);
	};

	render() {
		const { authenticated } = this.props.user;

		const likeButton = !authenticated ? (
			<Link to="/login">
				<MyButton tip="Like">
					<UnlikedIcon color="inherit" />
				</MyButton>
			</Link>
		) : this.likedScream() ? (
			<MyButton tip="Undo like" onClick={this.unlikeScream}>
				<LikedIcon color="inherit" />
			</MyButton>
		) : (
			<MyButton tip="Like scream" onClick={this.likeScream}>
				<UnlikedIcon color="inherit" />
			</MyButton>
		);
		return likeButton;
	}
}

LikeButton.propTypes = {
	user: PropTypes.object.isRequired,
	screamId: PropTypes.string.isRequired,
	likeScream: PropTypes.func.isRequired,
	unlikeScream: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user
});
const mapActionsToProps = {
	likeScream,
	unlikeScream
};

export default connect(
	mapStateToProps,
	mapActionsToProps
)(LikeButton);
