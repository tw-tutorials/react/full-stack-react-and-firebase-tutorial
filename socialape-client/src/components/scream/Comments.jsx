import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';

const styles = {
	invisibleSeparator: { border: 'none', margin: 4 },
	visibleSeparator: {
		width: '100%',
		borderBottom: '1px solid rgba(255,255,255,.1)',
		marginBottom: 20
	},
	commentImage: { maxWidth: '100%', height: 100, objectFit: 'cover', borderRadius: '50%' },
	commentData: { marginLeft: 20 }
};

class Comments extends Component {
	render() {
		const { classes, comments } = this.props;

		return (
			<Grid container>
				{comments.map((comment, index) => {
					const { body, createdAt, userImage, userHandle } = comment;
					return (
						<React.Fragment key={createdAt}>
							<Grid item sm={12}>
								<Grid container>
									<Grid item sm={2}>
										<img
											src={userImage}
											alt="comment"
											className={classes.commentImage}
										/>
									</Grid>
									<Grid item sm={9}>
										<div className={classes.commentData}>
											<Typography
												variant="h5"
												component={Link}
												color="inherit"
												to={`/users/${userHandle}`}
											>
												{userHandle}
											</Typography>
											<Typography variant="body2" color="textSecondary">
												{dayjs(createdAt).format('h:mm a, MMMM DD YYYY')}
											</Typography>
											<hr className={classes.invisibleSeparator} />
											<Typography variant="body1">{body}</Typography>
										</div>
									</Grid>
								</Grid>
							</Grid>
							{index !== comments.length - 1 && (
								<hr className={classes.visibleSeparator} />
							)}
						</React.Fragment>
					);
				})}
			</Grid>
		);
	}
}

Comments.propTypes = {
	classes: PropTypes.object.isRequired,
	comments: PropTypes.array.isRequired
};

export default withStyles(styles)(Comments);
