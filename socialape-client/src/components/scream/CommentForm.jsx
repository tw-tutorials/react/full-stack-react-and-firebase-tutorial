import React, { Component } from 'react';
import PropTypes from 'prop-types';

// MUI stutt
import { withStyles } from '@material-ui/core/styles';
import { Button, Grid, TextField } from '@material-ui/core';

// Redux
import { connect } from 'react-redux';
import { submitComment } from '../../redux/actions/data.actions';

const styles = {
	textField: { margin: '10px auto' },
	visibleSeparator: {
		width: '100%',
		borderBottom: '1px solid rgba(255,255,255,.1)',
		marginBottom: 20
	}
};

class CommentForm extends Component {
	state = {
		data: { body: '' },
		errors: {}
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.ui.errors) {
			this.setState({ errors: nextProps.ui.errors });
		}
		if (!nextProps.ui.errors && !nextProps.ui.loading) {
			this.setState({ data: { body: '' } });
		}
	}

	handleChange = ({ currentTarget: target }) => {
		const data = { ...this.state.data };
		data[target.name] = target.value;
		this.setState({ data });
	};
	handleSubmit = (e) => {
		e.preventDefault();
		this.props.submitComment(this.props.screamId, this.state.data);
	};

	render() {
		const { classes, authenticated } = this.props;
		const { errors } = this.state;

		const commentFormMarkup = authenticated ? (
			<Grid item sm={12} style={{ textAlign: 'center' }}>
				<form onSubmit={this.handleSubmit}>
					<TextField
						name="body"
						type="text"
						label="Comment on scream"
						error={errors.comment ? true : false}
						helperText={errors.comment}
						value={this.state.data.body}
						onChange={this.handleChange}
						fullWidth
						className={classes.textField}
					/>
					<Button
						type="submit"
						variant="contained"
						color="primary"
						className={classes.button}
					>
						Submit
					</Button>
				</form>
				<hr className={classes.visibleSeparator} />
			</Grid>
		) : null;
		return commentFormMarkup;
	}
}
CommentForm.propTypes = {
	classes: PropTypes.object.isRequired,
	submitComment: PropTypes.func.isRequired,
	ui: PropTypes.object.isRequired,
	authenticated: PropTypes.bool.isRequired,
	screamId: PropTypes.string.isRequired
};

const mapActionsToProps = (state) => ({
	ui: state.ui,
	authenticated: state.user.authenticated
});

export default connect(
	mapActionsToProps,
	{ submitComment }
)(withStyles(styles)(CommentForm));
