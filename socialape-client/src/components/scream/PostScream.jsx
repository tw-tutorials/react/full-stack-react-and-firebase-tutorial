import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MyButton from '../../util/MyButton';

// MUI stuff
import { withStyles } from '@material-ui/styles';
import {
	CircularProgress,
	Dialog,
	DialogTitle,
	DialogContent,
	TextField,
	Button
} from '@material-ui/core';

// Icons
import { Add as AddIcon } from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { postScream, clearErrors } from '../../redux/actions/data.actions';

const styles = {
	closeButton: { position: 'absolute', left: '90%', top: '10%' },
	textField: { margin: '10px auto' },
	submitButton: { position: 'relative', float: 'right', marginTop: 10 },
	progressSpinner: { position: 'absolute', left: '50%', right: '50%' }
};

class PostScream extends Component {
	state = {
		dialogOpen: false,
		data: {
			body: ''
		},
		errors: {}
	};

	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.ui.errors) {
			this.setState({ errors: nextProps.ui.errors });
		}
		if (!nextProps.ui.errors && !nextProps.ui.loading) {
			this.setState({ data: { body: '' }, dialogOpen: false, errors: {} });
		}
	}

	handleDialogOpen = () => {
		this.setState({ dialogOpen: true });
	};
	handleDialogClose = () => {
		this.props.clearErrors();
		this.setState({ dialogOpen: false, errors: {} });
	};

	handleChange = ({ currentTarget: target }) => {
		const data = { ...this.state.data };
		data[target.name] = target.value;
		this.setState({ data });
	};
	handleSubmit = (e) => {
		e.preventDefault();

		this.props.postScream(this.state.data);
	};

	render() {
		const { errors } = this.state;
		const {
			classes,
			ui: { loading }
		} = this.props;

		return (
			<React.Fragment>
				<MyButton tip="Create a scream" onClick={this.handleDialogOpen}>
					<AddIcon color="inherit" />
				</MyButton>
				<Dialog
					open={this.state.dialogOpen}
					onClose={this.handleDialogClose}
					fullWidth
					maxWidth="sm"
				>
					<DialogTitle>Post a scream</DialogTitle>
					<DialogContent>
						<form onSubmit={this.handleSubmit}>
							<TextField
								name="body"
								type="text"
								label="Scream"
								multiline
								rows="3"
								placeholder="Scream at your fellow apes"
								error={errors.body ? true : false}
								helperText={errors.body}
								className={classes.textField}
								onChange={this.handleChange}
								fullWidth
							/>
							<Button
								type="submit"
								variant="contained"
								color="primary"
								className={classes.submitButton}
								disabled={loading}
							>
								Scream
								{loading && (
									<CircularProgress
										size={24}
										className={classes.progressSpinner}
									/>
								)}
							</Button>
						</form>
					</DialogContent>
				</Dialog>
			</React.Fragment>
		);
	}
}

PostScream.propTypes = {
	classes: PropTypes.object.isRequired,
	postScream: PropTypes.func.isRequired,
	clearErrors: PropTypes.func.isRequired,
	ui: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	ui: state.ui
});

export default connect(
	mapStateToProps,
	{ postScream, clearErrors }
)(withStyles(styles)(PostScream));
