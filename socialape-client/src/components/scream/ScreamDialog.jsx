import React, { Component } from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import MyButton from '../../util/MyButton';
import { Link } from 'react-router-dom';
import LikeButton from './LikeButton';
import Comments from './Comments';

// MUI stuff
import { withStyles } from '@material-ui/styles';
import { Dialog, DialogContent, CircularProgress, Grid, Typography } from '@material-ui/core';

// Icons
import { UnfoldMore as UnfoldMoreIcon, Chat as CommentsIcon } from '@material-ui/icons';

// Redux
import { connect } from 'react-redux';
import { getScream, clearErrors } from '../../redux/actions/data.actions';
import CommentForm from './CommentForm';

const styles = {
	expandButton: { posotion: 'absolute', left: '90%' },
	closeButton: { posotion: 'absolute', left: '90%' },
	dialogContent: { padding: 20 },
	profileImage: { maxWidth: 200, height: 200, borderRadius: '50%', objectFit: 'cover' },
	invisibleSeparator: { border: 'none', margin: 4 },
	visibleSeparator: {
		width: '100%',
		borderBottom: '1px solid rgba(255,255,255,.1)',
		marginBottom: 20
	},
	spinnerDiv: { textAlign: 'center', marginTop: 50, marginBottom: 50 }
};

class ScreamDialog extends Component {
	state = {
		dialogOpen: false,
		oldPath: '',
		newPath: ''
	};

	componentDidMount() {
		if (this.props.openDialog) {
			this.handleDialogOpen();
		}
	}

	handleDialogOpen = () => {
		let oldPath = window.location.pathname;
		const { userHandle, screamId } = this.props;
		const newPath = `/users/${userHandle}/screams/${screamId}`;
		window.history.pushState(null, null, newPath);
		if (oldPath === newPath) oldPath = `/users/${userHandle}`;

		this.setState({ oldPath, newPath, dialogOpen: true });
		this.props.getScream(this.props.screamId);
	};
	handleDialogClose = () => {
		window.history.pushState(null, null, this.state.oldPath);

		this.setState({ dialogOpen: false });
		this.props.clearErrors();
	};

	render() {
		const {
			classes,
			scream: {
				screamId,
				body,
				createdAt,
				likeCount,
				commentCount,
				userImage,
				userHandle,
				comments
			},
			ui: { loading }
		} = this.props;

		const dialogMarkup = loading ? (
			<div className={classes.spinnerDiv}>
				<CircularProgress size={200} thickness={2} />
			</div>
		) : (
			<Grid container spacing={2}>
				<Grid item sm={5}>
					<img src={userImage} alt="Profile" className={classes.profileImage} />
				</Grid>
				<Grid item sm={7}>
					<Typography
						component={Link}
						color="inherit"
						variant="h5"
						to={`/users/${userHandle}`}
					>
						@{userHandle}
					</Typography>
					<hr className={classes.invisibleSeparator} />
					<Typography variant="body2" color="textSecondary">
						{dayjs(createdAt).format('h:mm a, MMMM DD YYYY')}
					</Typography>
					<hr className={classes.invisibleSeparator} />
					<Typography variant="body1">{body}</Typography>

					<LikeButton screamId={screamId} />
					<span>{likeCount} Likes</span>

					{/* Comments */}
					<MyButton tip="Comments">
						<CommentsIcon color="inherit" />
					</MyButton>
					<span>{commentCount} Comments</span>
				</Grid>
				<hr className={classes.visibleSeparator} />
				<CommentForm screamId={screamId} />
				{commentCount > 0 && <Comments comments={comments} />}
			</Grid>
		);

		return (
			<React.Fragment>
				<MyButton
					tip="Expand scream"
					onClick={this.handleDialogOpen}
					tipClassName={classes.expandButton}
				>
					<UnfoldMoreIcon />
				</MyButton>
				<Dialog
					open={this.state.dialogOpen}
					onClose={this.handleDialogClose}
					fullWidth
					maxWidth="sm"
				>
					<DialogContent className={classes.dialogContent}>{dialogMarkup}</DialogContent>
				</Dialog>
			</React.Fragment>
		);
	}
}

ScreamDialog.propTypes = {
	classes: PropTypes.object.isRequired,
	getScream: PropTypes.func.isRequired,
	screamId: PropTypes.string.isRequired,
	userHandle: PropTypes.string.isRequired,
	scream: PropTypes.object.isRequired,
	ui: PropTypes.object.isRequired,
	clearErrors: PropTypes.func.isRequired,
	openDialog: PropTypes.bool
};

const mapStateToProps = (state) => ({
	scream: state.data.scream,
	ui: state.ui
});
const mapActionsToProps = { getScream, clearErrors };

export default connect(
	mapStateToProps,
	mapActionsToProps
)(withStyles(styles)(ScreamDialog));
