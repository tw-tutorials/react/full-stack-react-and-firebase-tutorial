import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

// Redux stuff
import { connect } from 'react-redux';

const AuthRoute = ({ component: Component, user, ...rest }) => {
	return (
		<Route
			{...rest}
			render={(props) => {
				if (user.authenticated === true) {
					return <Redirect to="/" />;
				} else {
					return <Component {...props} />;
				}
			}}
		/>
	);
};

AuthRoute.propTypes = {
	user: PropTypes.object
};

const mapStateToProps = (state) => ({
	user: state.user
});

export default connect(mapStateToProps)(AuthRoute);
