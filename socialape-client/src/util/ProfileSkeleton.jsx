import React from 'react';
import PropTypes from 'prop-types';
import NoImg from '../images/no-img.png';

// MUI
import { withStyles } from '@material-ui/core/styles';
import { Card, CardMedia, CardContent, Paper } from '@material-ui/core';

//Icons
import {
	LocationOn as LocationOnIcon,
	Link as LinkIcon,
	CalendarToday as CalendarTodayIcon
} from '@material-ui/icons';

const styles = (theme) => ({
	paper: {
		padding: 20
	},
	profile: {
		'& .image-wrapper': {
			textAlign: 'center',
			position: 'relative'
		},
		'& .profile-image': {
			width: 200,
			height: 200,
			objectFit: 'cover',
			maxWidth: '100%',
			borderRadius: '50%'
		},
		'& .profile-details': {
			textAlign: 'center',
			'& span, svg': {
				verticalAlign: 'middle'
			},
			'& a': {
				color: theme.palette.primary.main
			}
		},
		'& hr': {
			border: 'none',
			margin: '0 0 10px 0'
		},
		'& svg.button': {
			'&:hover': {
				cursor: 'pointer'
			}
		}
	},
	handle: {
		width: 120,
		height: 20,
		backgroundColor: theme.palette.background.default,
		margin: '0 auto 7px auto',
		borderRadius: 2
	},
	fullLine: {
		width: '100%',
		height: 15,
		backgroundColor: theme.palette.background.default,
		margin: '0 auto 7px auto',
		borderRadius: 2
	},
	halfLine: {
		width: '50%',
		height: 15,
		backgroundColor: theme.palette.background.default,
		margin: '0 auto 7px auto',
		borderRadius: 2
	}
});

const ProfileSkeleton = (props) => {
	const { classes } = props;

	return (
		<Paper className={classes.paper}>
			<div className={classes.profile}>
				<div className="image-wrapper">
					<img src={NoImg} alt="profile" className="profile-image" />
				</div>
				<hr />
				<div className="profile-details">
					<div className={classes.handle} />
					<hr />
					<div className={classes.fullLine} />
					<div className={classes.fullLine} />
					<hr />
					<LocationOnIcon color="inherit" /> <span>Location</span>
					<hr />
					<LinkIcon color="inherit" /> <span>https://website.com</span>
					<hr />
					<CalendarTodayIcon color="inherit" /> <span>Joined date</span>
				</div>
			</div>
		</Paper>
	);
};

ProfileSkeleton.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ProfileSkeleton);
