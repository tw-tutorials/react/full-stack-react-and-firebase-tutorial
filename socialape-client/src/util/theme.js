import { indigo, pink, red } from '@material-ui/core/colors';
export default {
	palette: {
		primary: indigo,
		secondary: pink,
		error: red,
		type: 'dark'
	}
};
