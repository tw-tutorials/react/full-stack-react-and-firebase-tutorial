import React from 'react';
import PropTypes from 'prop-types';
import NoImg from '../images/no-img.png';

// MUI
import { withStyles } from '@material-ui/core/styles';
import { Card, CardMedia, CardContent } from '@material-ui/core';

const styles = (theme) => ({
	card: { display: 'flex', marginBottom: 20 },
	cardContent: { width: '100%', flexDirection: 'column', padding: 25 },
	cover: { minWidth: 200, objectFit: 'cover' },
	handle: {
		width: 120,
		height: 18,
		backgroundColor: theme.palette.background.default,
		marginBottom: 7,
		borderRadius: 2
	},
	date: {
		width: 100,
		height: 14,
		backgroundColor: theme.palette.background.default,
		marginBottom: 10,
		borderRadius: 2
	},
	fullLine: {
		width: '90%',
		height: 15,
		backgroundColor: theme.palette.background.default,
		marginBottom: 7,
		borderRadius: 2
	},
	halfLine: {
		width: '45%',
		height: 15,
		backgroundColor: theme.palette.background.default,
		marginBottom: 7,
		borderRadius: 2
	}
});

const ScreamSkeleton = (props) => {
	const { classes } = props;

	const content = Array.from({ length: 5 }).map((item, index) => (
		<Card className={classes.card} key={index}>
			<CardMedia className={classes.cover} image={NoImg} />
			<CardContent className={classes.cardContent}>
				<div className={classes.handle} />
				<div className={classes.date} />
				<div className={classes.fullLine} />
				<div className={classes.fullLine} />
				<div className={classes.halfLine} />
			</CardContent>
		</Card>
	));

	return <React.Fragment>{content}</React.Fragment>;
};

ScreamSkeleton.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ScreamSkeleton);
