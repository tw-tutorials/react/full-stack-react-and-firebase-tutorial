import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import theme from './util/theme';
import jwtDecode from 'jwt-decode';
import axios from 'axios';

// Redux
import { Provider } from 'react-redux';
import store from './redux/store';
import { SET_AUTHENTICATED } from './redux/types';
import { logoutUser, getUserData } from './redux/actions/user.actions';

// Components
import AuthRoute from './util/AuthRoute';
import NavBar from './components/layout/NavBar';

// Pages
import home from './pages/home';
import login from './pages/login';
import signup from './pages/signup';
import { CssBaseline } from '@material-ui/core';
import user from './pages/user';

// Theme
const muiTheme = createMuiTheme(theme);

axios.defaults.baseURL = 'https://europe-west1-tw-socialape.cloudfunctions.net/api';

const token = localStorage.getItem('x-auth-token');
if (token) {
	const decodedToken = jwtDecode(token);
	if (decodedToken.exp * 1000 < Date.now()) {
		store.dispatch(logoutUser());
		window.location.href = '/login';
	} else {
		store.dispatch({ type: SET_AUTHENTICATED });
		axios.defaults.headers.common['Authorization'] = token;
		store.dispatch(getUserData());
	}
}

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<MuiThemeProvider theme={muiTheme}>
					<div className="App">
						<BrowserRouter>
							<CssBaseline />
							<NavBar />
							<div className="container">
								<Switch>
									<Route exact path="/" component={home} />
									<AuthRoute exact path="/login" component={login} />
									<AuthRoute exact path="/signup" component={signup} />
									<Route exact path="/users/:handle" component={user} />
									<Route
										exact
										path="/users/:handle/screams/:screamId"
										component={user}
									/>
								</Switch>
							</div>
						</BrowserRouter>
					</div>
				</MuiThemeProvider>
			</Provider>
		);
	}
}

export default App;
