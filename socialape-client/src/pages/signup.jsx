import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AppIcon from '../images/icon.png';

// MUI stuff
import { withStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import { Grid, Typography, TextField, Button, CircularProgress } from '@material-ui/core';

// Redux stuff
import { connect } from 'react-redux';
import { signupUser } from '../redux/actions/user.actions';

const styles = (theme) => ({
	form: { textAlign: 'center' },
	image: { margin: '20px auto' },
	pageTitle: { margin: '10px auto' },
	textField: { margin: '10px auto' },
	customError: { color: red[500], fontSize: '.8rem', marginTop: 10 },
	button: { marginTop: 20, position: 'relative' },
	progress: { position: 'absolute' }
});

class signup extends Component {
	constructor() {
		super();
		this.state = {
			data: {
				email: '',
				password: '',
				confirmPassword: '',
				handle: ''
			},
			errors: {},
			loading: false
		};
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.ui.errors) {
			this.setState({ errors: nextProps.ui.errors });
		}
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.signupUser(this.state.data, this.props.history);

		// this.setState({ loading: true });
		// axios
		// 	.post('/signup', this.state.data)
		// 	.then((res) => {
		// 		console.log(res.data);
		// 		localStorage.setItem('x-auth-token', `Bearer ${res.data.token}`);
		// 		this.setState({ loading: false });
		// 		this.props.history.push('/');
		// 	})
		// 	.catch((err) => {
		// 		this.setState({
		// 			errors: err.response.data,
		// 			loading: false
		// 		});
		// 	});
	};
	handleChange = ({ currentTarget: target }) => {
		const data = { ...this.state.data };
		data[target.name] = target.value;
		this.setState({ data });
	};

	render() {
		const {
			classes,
			ui: { loading }
		} = this.props;
		const { errors } = this.state;

		return (
			<Grid container className={classes.form}>
				<Grid item sm />
				<Grid item sm>
					<img src={AppIcon} alt="monkey" className={classes.image} />
					<Typography variant="h2" className={classes.pageTitle}>
						Signup
					</Typography>
					<form noValidate onSubmit={this.handleSubmit}>
						<TextField
							id="email"
							name="email"
							type="email"
							label="Email"
							autoComplete="email"
							className={classes.textField}
							helperText={errors.email}
							error={errors.email ? true : false}
							value={this.state.data.email}
							onChange={this.handleChange}
							fullWidth
						/>
						<TextField
							id="password"
							name="password"
							type="password"
							label="Password"
							autoComplete="password"
							className={classes.textField}
							helperText={errors.password}
							error={errors.password ? true : false}
							value={this.state.data.password}
							onChange={this.handleChange}
							fullWidth
						/>
						<TextField
							id="confirmPassword"
							name="confirmPassword"
							type="password"
							label="Confirm Password"
							autoComplete="password"
							className={classes.textField}
							helperText={errors.confirmPassword}
							error={errors.confirmPassword ? true : false}
							value={this.state.data.confirmPassword}
							onChange={this.handleChange}
							fullWidth
						/>
						<TextField
							id="handle"
							name="handle"
							type="username"
							label="Username"
							autoComplete="username"
							className={classes.textField}
							helperText={errors.handle}
							error={errors.handle ? true : false}
							value={this.state.data.handle}
							onChange={this.handleChange}
							fullWidth
						/>

						{errors.general && (
							<Typography variant="body2" className={classes.customError}>
								{errors.general}
							</Typography>
						)}
						<Button
							type="submit"
							variant="contained"
							color="primary"
							className={classes.button}
							disabled={loading}
						>
							Signup
							{loading && (
								<CircularProgress
									size={24}
									className={classes.progress}
									color="secondary"
								/>
							)}
						</Button>
						<br />

						<small>
							Already have an account? Login{' '}
							<Typography to="/login" component={Link} color="secondary">
								<small>here</small>
							</Typography>
							.
						</small>
					</form>
				</Grid>
				<Grid item sm />
			</Grid>
		);
	}
}

signup.propTypes = {
	classes: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired,
	ui: PropTypes.object.isRequired,
	signupUser: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
	user: state.user,
	ui: state.ui
});
const mapActionsToProps = { signupUser };

export default connect(
	mapStateToProps,
	mapActionsToProps
)(withStyles(styles)(signup));
