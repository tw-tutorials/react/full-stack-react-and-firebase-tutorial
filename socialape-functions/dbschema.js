let db = {
	users: [
		{
			userId: 'TZH6dfcnRtc31NVEa7Ak0QuLShj1',
			email: 'user@email.com',
			handle: 'user',
			createdAt: '2019-08-14T12:42:42.459Z',
			imageUrl:
				'https://firebasestorage.googleapis.com/v0/b/tw-socialape.appspot.com/o/4488629785.jpg?alt=media',
			bio: 'Hello, my name is user, nice to meet you',
			website: 'https://user.com',
			location: 'London, UK'
		}
	],
	screams: [
		{
			userHandle: 'user',
			body: 'this is the scream body',
			createdAt: '2019-08-14T02:22:35.781Z',
			likeCount: 5,
			commentCount: 2
		}
	],
	comments: [
		{
			userHandle: 'user',
			screamId: '7lRPcVMEbEqheHehwJiR',
			body: 'nice one mate!',
			createdAt: '2019-08-15T02:22:35.781Z'
		}
	],
	notifications: [
		{
			recipient: 'user',
			sender: 'john',
			read: 'true | false',
			screamId: '7lRPcVMEbEqheHehwJiR',
			type: 'like | comment',
			createdAt: '2019-08-15T02:22:35.781Z'
		}
	]
};
const userDetails = {
	// Redux data
	credentials: {
		userId: 'TZH6dfcnRtc31NVEa7Ak0QuLShj1',
		email: 'user@email.com',
		handle: 'user',
		createdAt: '2019-08-14T12:42:42.459Z',
		imageUrl:
			'https://firebasestorage.googleapis.com/v0/b/tw-socialape.appspot.com/o/4488629785.jpg?alt=media',
		bio: 'Hello, my name is user, nice to meet you',
		website: 'https://user.com',
		location: 'London, UK'
	},
	likes: [
		{
			userHandle: 'user',
			screamId: '7lRPcVMEbEqheHehwJiR'
		},
		{
			userHandle: 'user',
			screamId: 'x2WDqp0lEKWkFv6ejVrB'
		}
	]
};
