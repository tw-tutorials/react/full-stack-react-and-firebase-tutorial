const functions = require('firebase-functions');
const app = require('express')();

// middleware
const auth = require('./middleware/auth');

const cors = require('cors');
app.use(cors());

const { db } = require('./util/admin');

// handlers
const {
	getAllScreams,
	postOneScream,
	getScream,
	deleteScream,
	commentOnScream,
	likeScream,
	unlikeScream
} = require('./handlers/screams');
const {
	signup,
	login,
	addUserDetails,
	getAuthenticatedUser,
	uploadImage,
	getUserDetails,
	markNotificationsRead
} = require('./handlers/users');

//#region Routes

// scream routes
app.get('/screams', getAllScreams);
app.post('/screams', [auth], postOneScream);
app.get('/screams/:screamId', getScream);
app.delete('/screams/:screamId', [auth], deleteScream);
app.get('/screams/:screamId/like', [auth], likeScream);
app.get('/screams/:screamId/unlike', [auth], unlikeScream);
app.post('/screams/:screamId/comment', [auth], commentOnScream);

// user routes
app.post('/signup', signup);
app.post('/login', login);
app.post('/user', [auth], addUserDetails);
app.post('/user/image', [auth], uploadImage);
app.get('/user', [auth], getAuthenticatedUser);
app.get('/users/:handle', getUserDetails);
app.post('/notifications', [auth], markNotificationsRead);

//#endregion

exports.api = functions.region('europe-west1').https.onRequest(app);

exports.createNotificationOnLike = functions
	.region('europe-west1')
	.firestore.document('likes/{id}')
	.onCreate((snapshot) => {
		return db
			.doc(`/screams/${snapshot.data().screamId}`)
			.get()
			.then((doc) => {
				if (doc.exists && doc.data().userHandle !== snapshot.data().userHandle) {
					return db.doc(`/notifications/${snapshot.id}`).set({
						createdAt: new Date().toISOString(),
						recipient: doc.data().userHandle,
						sender: snapshot.data().userHandle,
						type: 'like',
						read: false,
						screamId: doc.id
					});
				}
			})
			.catch((err) => {
				console.error(err);
			});
	});
exports.createNotificationOnUnlike = functions
	.region('europe-west1')
	.firestore.document('likes/{id}')
	.onDelete((snapshot) => {
		return db
			.doc(`/notifications/${snapshot.id}`)
			.delete()
			.catch((err) => {
				console.error(err);
			});
	});

exports.createNotificationOnComment = functions
	.region('europe-west1')
	.firestore.document('comments/{id}')
	.onCreate((snapshot) => {
		return db
			.doc(`/screams/${snapshot.data().screamId}`)
			.get()
			.then((doc) => {
				if (doc.exists) {
					return db.doc(`/notifications/${snapshot.id}`).set({
						createdAt: new Date().toISOString(),
						recipient: doc.data().userHandle,
						sender: snapshot.data().userHandle,
						type: 'comment',
						read: false,
						screamId: doc.id
					});
				}
			})
			.catch((err) => {
				console.error(err);
			});
	});

exports.onUserImageChange = functions
	.region('europe-west1')
	.firestore.document('/users/{userId}')
	.onUpdate((change) => {
		if (change.before.data().imageUrl !== change.after.data().imageUrl) {
			const batch = db.batch();
			return db
				.collection('screams')
				.where('userHandle', '==', change.before.data().handle)
				.get()
				.then((data) => {
					data.forEach((doc) => {
						const scream = db.doc(`/screams&${doc.id}`);
						batch.update(scream, { userImage: change.after.data().imageUrl });
					});
					return batch.commit();
				});
		} else {
			return true;
		}
	});

exports.onScreamDelete = functions
	.region('europe-west1')
	.firestore.document('/screams/{screamId}')
	.onDelete((snapshot, context) => {
		const screamId = context.params.screamId;
		const batch = db.batch();
		return db
			.collection('comments')
			.where('screamId', '==', screamId)
			.get()
			.then((data) => {
				data.forEach((doc) => {
					batch.delete(db.doc(`/comments/${doc.id}`));
				});
				return db
					.collection('likes')
					.where('screamId', '==', screamId)
					.get();
			})
			.then((data) => {
				data.forEach((doc) => {
					batch.delete(db.doc(`/likes/${doc.id}`));
				});
				return db
					.collection('notifications')
					.where('screamId', '==', screamId)
					.get();
			})
			.then((data) => {
				data.forEach((doc) => {
					batch.delete(db.doc(`/notifications/${doc.id}`));
				});
				return batch.commit();
			})
			.catch((err) => {
				console.error(err);
			});
	});
