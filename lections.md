# Full Stack React & Firebase Tutorial
- 1 - Introduction
- 2 - Create & Read Data
- 3 - Express & formatting response
- 4 - User Registration
- 5 - Validation & Login Route
- 6 - Authentication Middleware
- 7 - Refactoring & Organising
- 8 - Image Upload
- 9 - Add and Get User Profile Details
- 10 - Getting and Commenting on Scream
- 11 - Like, Unlike and Delete Scream
- 12 - Create and Get Notifications
- 13 - Finishing up Cloud Functions
- 14 - Getting Started With React
- 15 - Scream Card Details
- 16 - Login Form
- 17 - Signup and Auth State
- 18 - Redux Setup
- 19 - Signup and Auth Route
- 20 - Profile Section
- 21 - Image Upload
- 22 - Logout and Edit profile
- 23 - Navbar Buttons
- 24 - Like and Unlike Actions
- 25 - Delete Button
- 26 - Add Scream Component
- 27 - Scream Dialog
- 28 - Scream Dialog Details
- 29 - Displaying Comments
- 30 - Submitting comments
- 31 - User Page
- 32 - Notifications
- 33 - Loading Skeletons
- 34 - Deployment to Firebase